package ro.orangeStartIT;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
	List<String> alpha = Arrays.asList("p", "i", "z", "z", "a");// A list with strings called alpha and create a new ArrayList
	System.out.println(alpha);// Add 4 strings(letters) in the list and print it

        Object uppercase = alpha.stream().map(String::toUpperCase).collect(Collectors.toList());
        //Convert the list to stream
        //Use .map to switch strings to upper case
        //Use the terminal operation .collector to transform the elements of the stream into a list
        System.out.println(uppercase);  //Print the new list

        List<Integer> nums = Arrays.asList(1, 2, 3, 4, 5); //Create another list and add 5 integers
        System.out.println((nums));
        Object multiply = nums.stream().map(a -> a*2).collect(Collectors.toList());
        //Convert the list to stream and use .map to multiply by 2 each integer.
        // Use the terminal operation .collector to transform the elements of the stream into a list
        System.out.println((multiply));
    }
}